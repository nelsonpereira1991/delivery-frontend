import {BrowserRouter as Router, Route, Link, Switch} from 'react-router-dom'
import {HomePage, NotFoundPage, AdminPage, UpdateStorePage, CreateOrderPage} from './pages'
import './App.css';


function App() {
  return (
    <div className="App">
      <Router>
        <Link to='/admin'> Go to Admin Page</Link>
        <Link to='/'> Go to Home Page</Link>
        <Switch>
          <Route path='/' exact>
            <HomePage/>
          </Route>
          <Route path='/admin'>
            <AdminPage />
          </Route>
          <Route path='/update-store'>
            <UpdateStorePage />
          </Route>
          <Route path='/create-order'>
            <CreateOrderPage />
          </Route>
          <Route>
            <NotFoundPage/>
          </Route>
        </Switch>
      </Router>
      <p hidden> Learn React </p>
    </div>
  );
}

export default App;
