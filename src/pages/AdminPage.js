import React, {useState, useEffect} from 'react'
import {Link, useHistory} from 'react-router-dom'
import axios from 'axios'
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';


const useStyles = makeStyles((theme) => ({
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: 100,
    },
}));

const createStoreRequest = async({name, openTime, closeTime}) => {    
    const response = await axios({
      method: 'post',
      url: `${process.env.REACT_APP_API_BASE_URL}/api/admin/stores`,
      data: {
        name,
        openTime,
        closeTime
      }
    });
    return response
}


const getStoresRequest = async() => {    
    const response = await axios({
      method: 'get',
      url: `${process.env.REACT_APP_API_BASE_URL}/api/stores`
    });
    return response.data
}

export const AdminPage = () => {
    const classes = useStyles();
    const [store, setStore] = useState({name: '', openTime: '', closeTime: ''})
    const [stores, setStores] = useState([])
    const history = useHistory()

    const fetchStores = async() => {
        const stores = await getStoresRequest()
        setStores(stores)
    }

    const handleChange = (event) => {
        console.log('Im here event.target.name: ' + event.target.name)
        console.log('Im here event.target.value: ' + event.target.value)
        setStore({...store, [event.target.name]: event.target.value})
    }

    const handleSubmit = async(evt) => {
        evt.preventDefault();
        console.log('The store to be created is: ', store)
        const response = await createStoreRequest(store)
        alert(`Created ${response.data.name}`)
        history.push('/')
    }

    useEffect(() => {
        fetchStores();
      }, []);


      /*
      <label htmlFor='openTime'> Open time </label>
                <input type='text' name='openTime' onChange={handleChange}/>
                */

      /*
      <label htmlFor='closeTime'> Close Time </label>
                <input type='text' name='closeTime' onChange={handleChange}/>
                */

    return (
        <>
            <h1>The Admin page</h1>
            <p>This is the Admin page!</p>
            <form className={classes.container} onSubmit={handleSubmit}>
                <label htmlFor='name'> Store Name </label>
                <input type='text' name='name' onChange={handleChange}/>
                <TextField
                    name='openTime'
                    onChange={handleChange}
                    id="time"
                    label="Open time"
                    type="time"
                    defaultValue="08:00"
                    className={classes.textField}
                    InputLabelProps={{
                    shrink: true,
                    }}
                    inputProps={{
                    step: 300, // 5 min
                    }}
                />
                <TextField
                    name='closeTime'
                    onChange={handleChange}
                    id="time"
                    label="Close time"
                    type="time"
                    defaultValue="22:00"
                    className={classes.textField}
                    InputLabelProps={{
                    shrink: true,
                    }}
                    inputProps={{
                    step: 300, // 5 min
                    }}
                />
                <button> Create Store </button>
            </form>

            <>
                {stores.map((storeElement) => (
                    <div key={storeElement.id}>
                        <br></br>
                        <Link
                        to={{
                            pathname: "update-store",
                            state: {id: storeElement.id, name: storeElement.name, openTime: storeElement.openTime, closeTime: storeElement.closeTime}
                        }}
                        > Name: {storeElement.name} </Link>
                        <p> Open Time: {storeElement.openTime} </p>
                        <p> Close Time: {storeElement.closeTime} </p>
                        <br></br>
                    </div>
                ))}
            </>
        </>
        

    )
}