import React, {useState} from 'react'
import {useLocation, useHistory} from 'react-router-dom'
import axios from 'axios'
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';


const useStyles = makeStyles((theme) => ({
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: 100,
    },
}));

const updateStoreRequest = async({id, name, openTime, closeTime}) => {    
    const response = await axios({
      method: 'put',
      url: `${process.env.REACT_APP_API_BASE_URL}/api/admin/stores/${id}`,
      data: {
        name,
        openTime,
        closeTime
      }
    });
    return response
}

export const UpdateStorePage = () => {
    const classes = useStyles();
    const { state } = useLocation();
    const [store, setStore] = useState(state)
    const history = useHistory()
    console.log('the store is: ', store)

    const handleChange = (event) => {
        setStore({...store, [event.target.name]: event.target.value})
    }

    const handleSubmit = async(evt) => {
        evt.preventDefault();
        console.log('The store to be updated is: ', store)
        const response = await updateStoreRequest(store)
        alert(`Updated ${response.data.name}`)
        history.push('/')
    }

    return (
        <>
            <h1>Update Store Page</h1>
            <p>Edit Store!</p>
            <form onSubmit={handleSubmit}>
                <label htmlFor='name'> Store Name </label>
                <input type='text' name='name' value={store.name} onChange={handleChange}/>
                <TextField
                    name='openTime'
                    onChange={handleChange}
                    id="time"
                    label="Open time"
                    type="time"
                    defaultValue={store.openTime}
                    className={classes.textField}
                    InputLabelProps={{
                    shrink: true,
                    }}
                    inputProps={{
                    step: 300, // 5 min
                    }}
                />
                <TextField
                    name='closeTime'
                    onChange={handleChange}
                    id="time"
                    label="Close time"
                    type="time"
                    defaultValue={store.closeTime}
                    className={classes.textField}
                    InputLabelProps={{
                    shrink: true,
                    }}
                    inputProps={{
                    step: 300, // 5 min
                    }}
                />
                <button> Update Store </button>
            </form>
        </>
        

    )
}