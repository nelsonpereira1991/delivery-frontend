import React, {useState, useEffect} from 'react'
import {useLocation, useHistory} from 'react-router-dom'
import axios from 'axios'

const createOrderRequest = async({id}) => {
    console.log('The slot id is: ', id)
    const response = await axios({
      method: 'post',
      url: `${process.env.REACT_APP_API_BASE_URL}/api/orders`,
      data: {
        id
      }
    });
    return response.data
}

const getAvailableSlotsForStore = async (storeId) => {
    const response = await axios({
        method: 'get',
        url: `${process.env.REACT_APP_API_BASE_URL}/api/available-slots?storeId=${storeId}`
      });
      return response.data
}

export const CreateOrderPage = () => {
    const { state } = useLocation();
    //const [store, setStore] = useState(state)
    const [store] = useState(state)
    const [availableSlots, setAvailableSlots] = useState([])
    const history = useHistory()
    console.log('the store is: ', store)


    const fetchAvailableSlots = async(storeId) => {
        const availableSlots = await getAvailableSlotsForStore(storeId)
        setAvailableSlots(availableSlots)
    }

    const handleButtonClick = async (slot) => {
        console.log('The slot to be used for create order is: ', slot)
        //alert(`Updated ${response.data.name}`)
        const {id} = slot
        const result = await createOrderRequest({id})
        alert('Order delivered status: ' + result)
        history.push('/')
    }

    
    useEffect(() => {
        fetchAvailableSlots(store.id);
      }, [store]);


    return (
        <>
            <h1>Create Order Page</h1>
            <p>Available times for {store.name}!</p>
            <>
                {availableSlots.map((slotElement, index) => (
                    <div key={slotElement.id}>
                        <br></br>
                        <p> Begin time: {slotElement.beginTime}</p>
                        <p> End time: {slotElement.endTime} </p>
                        <p> Remaining capacity: {slotElement.remainingCapacity} </p>
                        <button onClick={() => handleButtonClick(slotElement)}> Create Order </button>
                        <br></br>
                    </div>
                ))}
            </>
            
        </>
        

    )
}