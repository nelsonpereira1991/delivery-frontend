import React, {useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import axios from 'axios'

const getStoresRequest = async() => {    
    const response = await axios({
      method: 'get',
      url: `${process.env.REACT_APP_API_BASE_URL}/api/stores`
    });
    return response.data
}

export const HomePage = () => {
    const [stores, setStores] = useState([])
    const fetchStores = async() => {
        const stores = await getStoresRequest()
        setStores(stores)
    }

    useEffect(() => {
        fetchStores();
      }, []);

    return (
        <>
            <h1>The home page</h1>
            <p>This is the home page!</p>

            <>
                {stores.map((storeElement) => (
                    <div key={storeElement.id}>
                        <br></br>
                        <Link
                        to={{
                            pathname: "create-order",
                            state: {id: storeElement.id, name: storeElement.name, openTime: storeElement.openTime, closeTime: storeElement.closeTime}
                        }}
                        > Name: {storeElement.name} </Link>
                        <p> Open Time: {storeElement.openTime} </p>
                        <p> Close Time: {storeElement.closeTime} </p>
                        <br></br>
                    </div>
                ))}
            </>
        </>
    )
}